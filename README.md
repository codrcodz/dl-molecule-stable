# dl-molecule-stable

Similar to the official molecule image, but uses the latest stable versions of molecule and ansible from pip.

## Usage

First, `cd` into the top-level directory containing your ansible content (playbooks, roles, et cetera).

Next, pull and run the docker container in this repository, making should to mount in your ansible content directory:
 
```
docker run --rm -it -v $PWD:/ansible-content 
