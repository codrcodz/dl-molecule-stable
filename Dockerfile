FROM alpine:latest
ENV BUILD_DEPS="\
                python3-dev \
                build-base \
                automake \
                autoconf \
                libffi-dev \
                openssl-dev \
               "
ENV SYS_DEPS="\
              python3 \
              python2 \
              ca-certificates \
              coreutils \
              openssh-client \
              git \
              bash \
              curl \
              docker-cli \
              py3-pip \
              py-pip \
              ${BUILD_DEPS} \
             "
ENV PY3_DEPS="\
              wheel \
              ansible \
              molecule \
              docker \
              ansible-lint \
              flake8 \
              molecule-containers \
              molecule-digitalocean \
              molecule-docker \
              molecule-libvirt \
              molecule-azure \
              molecule-ec2 \
              molecule-gce \
              molecule-lxd \
              molecule-openstack \
              molecule-vagrant \
              molecule-podman \
             "
ENV PY2_DEPS="\
              docker-py \
             "
ENV BINS="\
          python3 \
          python2 \
          molecule \
          ansible \
          ansible-galaxy \
          ansible-lint \
         "
ENV ANSIBLE_NAMESPACES="\
                        ansible \
                        community \
                       "
RUN { echo -e "\n[INFO] Displaying distro details:"; \
      cat /etc/*release || exit 1; } && \
    { echo -e "\n[INFO] Updating system package list:"; \
      apk -v update || exit 1; } && \
    { { echo '[global]' > /etc/pip.conf || exit 1; } && \
      { echo 'disable-pip-version-check = True' >> /etc/pip.conf || exit 1; }; } && \
    { for SYS_DEP in ${SYS_DEPS}; do \
        echo -e "\n[INFO] Installing sys dep: ${SYS_DEP}"; \
        apk -v add --no-cache "${SYS_DEP}"; \
      done || exit 1; } && \
    { echo -e "\n[INFO] Installing Python3 deps: ${PY3_DEPS}" && \
      pip3 install --no-cache-dir ${PY3_DEPS}; } || { exit 1; } && \
    { echo -e "\n[INFO] Installing Python2 deps: ${PY2_DEPS}" && \
      pip install --no-cache-dir ${PY2_DEPS}; } || { exit 1; } && \
    { echo -e "\n[INFO] Updating outdated Python3 packages:"; \
      for PKG in $(pip3 list --outdated --format=freeze); do \
        echo -e "${PKG}: $(pip3 install --upgrade ${PKG})\n"; \
      done; } && \
    { echo -e "\n[INFO] Updating outdated Python2 packages:"; \
      for PKG in $(pip list --outdated --format=freeze); do \
        echo -e "${PKG}: $(pip install --upgrade ${PKG})\n"; \
      done; } && \
    { echo -e "\n[INFO] Displaying Python3 packages installed list:"; \
      pip3 freeze || exit 1; } && \
    { echo -e "\n[INFO] Displaying Python2 packages installed list:"; \
      pip freeze || exit 1; } && \
    { for BUILD_DEP in ${BUILD_DEPS}; do \
        echo -e "\n[INFO] Deleting build dep: ${BUILD_DEP}"; \ 
        apk -v del ${BUILD_DEP}; \
      done || exit 1; } && \
    { echo -e "\n[INFO] Displaying sys packages stats and installed list:"; \
      { apt -v stats; apk -v list --installed; } || exit 1; } && \
    { for BIN in ${BINS}; do \
        echo -e "\n[INFO] Displaying location and version of bin: ${BIN}"; \
        { which ${BIN} || exit 1; }; \
      done || exit 1; };
SHELL ["/bin/bash", "--login", "-c"]
RUN { echo -e "\n[INFO] Installing all collections from commonly used namespaces."; \
      declare -A collections; \
      for namespace in ${ANSIBLE_NAMESPACES[@]}; do \
        namespaces+="^${namespace}.*|"; \
      done; \
      while read -r name desc; do \
        if [[ $name =~ ${namespaces::-1} ]]; then \
          collections[${BASH_REMATCH%.*}]="${BASH_REMATCH%.*}"; \
        fi; \
      done < <(ansible-doc -l 2>/dev/null); \
      for collection in "${collections[@]}"; do \
        echo -e "\n[INFO] Attempting to install collection: ${collection}"; \
        ansible-galaxy collection install "${collection}" || exit 1; \
      done; };
ADD molecule.yml /workdir/molecule.yml
ADD run_tests.sh /usr/local/bin/run_tests
RUN { echo -e "\n[INFO] Making run_tests script executable."; \
      { chmod +x /usr/local/bin/run_tests; } || exit 1; };
WORKDIR /workdir
CMD /bin/bash
