#!/bin/bash

workdir="${1}";
workdir="${workdir:=/workdir}";

if [[ ! -d "${workdir}" ]]; then
  echo -e "\n[FAIL] Specified workdir (${workdir}) invalid directory; exiting.";
  exit 1;
fi && \
{ echo -e "\n[INFO] Changing directories into workdir (${workdir})." && \
  cd ${workdir} && \
  echo -e "\n[INFO] Initializing role (testrole) with molecule." && \
  molecule init role testrole || exit 1; } && \
{ echo -e "\n[INFO] Overriding molecule.yml in default scenario directory with valid content." && \
  cp -f ${workdir}/molecule.yml ${workdir}/testrole/molecule/default/molecule.yml && \
  cd ${workdir}/testrole/ || exit 1; } && \
{ echo -e "\n[INFO] Running default molecule test command as smoke test." && \
  molecule test || exit 1; };
